﻿using System;
using System.Collections.Generic;

namespace Cards
{
    /*
     * Spades, Hearts, Clubs, Diamonds
     */
    public class Deck
    {
        private List<Card> _cards = new List<Card>();
        private Random _rand;

        public Deck()
        {
            _rand = new Random(DateTime.Now.Millisecond);
            Init();
        }

        public Deck(int rngSeed)
        {
            _rand = new Random(rngSeed);
            Init();
        }

        public List<Card> Deal(int numberCOfCards)
        {
            var hand = new List<Card>();

            if (numberCOfCards > 0 && numberCOfCards < _cards.Count)
            {
                Card card = null;

                for (int i = 0; i < numberCOfCards; i++)
                {
                    hand.Add(DealCard());
                }
            }

            return hand;
        }

        public Card DealCard()
        {
            Card card = null;
            var deckCount = _cards.Count;

            if (deckCount > 0)
            {
                int deal = _rand.Next(0, deckCount);

                card = _cards[deal];
                _cards.RemoveAt(deal);
            }

            return card;
        }

        public void ReturnCard(Card card)
        {
            _cards.Add(card);
        }

        public void Shuffle()
        {
            _cards.Clear();
            Init();
        }

        private void AddCards(Suit suit)
        {
            // Ace = 1, ..., King = 13
            for (int i = 1; i <= 13; i++)
                _cards.Add(new Card { Suit = suit, Value = i });
        }

        private void Init()
        {
            AddCards(Suit.Spades);
            AddCards(Suit.Hearts);
            AddCards(Suit.Clubs);
            AddCards(Suit.Diamonds);
        }
    }

    public class Card
    {
        public Suit Suit { get; set; }
        public int Value { get; set; }
    }

    public enum Suit
    {
        Spades,
        Hearts,
        Clubs,
        Diamonds
    }
}
