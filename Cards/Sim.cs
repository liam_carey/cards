﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cards
{
    public enum CardSimResultType
    {
        Max1,
        Min2,
        Min3,
        Min4,
        Sum2,
        Sum3,
        Sum4,
        SumMax2,
        SumMax3,
        SumWithReturn
    }

    public enum CardSimType
    {
        SingleThread,
        MultiThread
    }

    class CardsSim
    {
        public void Run()
        {
            Run(1000000);
        }

        public void Run(int n)
        {
            Run(n, CardSimType.SingleThread);
        }

        public void Run(int n, CardSimType simType)
        {
            var start = DateTime.Now;
            Console.WriteLine("--------------");
            Console.WriteLine("Starting simulation with n = {0}", n.ToString("#,###"));
            Console.WriteLine("--------------");

            if (n > 0)
            {
                if (simType.Equals(CardSimType.MultiThread))
                {
                    Console.WriteLine("Multi-threaded ...");
                    Console.WriteLine("--------------");
                    int m = n / 4;
                    List<CardSimResult> results = new List<CardSimResult>();
                    Task<CardSimResult> task1 = Task<CardSimResult>.Factory.StartNew(() => RunSimulation(m, 1));
                    Task<CardSimResult> task2 = Task<CardSimResult>.Factory.StartNew(() => RunSimulation(m, 2));
                    Task<CardSimResult> task3 = Task<CardSimResult>.Factory.StartNew(() => RunSimulation(m, 3));
                    Task<CardSimResult> task4 = Task<CardSimResult>.Factory.StartNew(() => RunSimulation((n - 3 * m), 4));
                    var result1 = task1.Result;
                    var result2 = task2.Result;
                    var result3 = task3.Result;
                    var result4 = task4.Result;
                    results.Add(result1);
                    results.Add(result2);
                    results.Add(result3);
                    results.Add(result4);

                    var aggregate = AggregateResults(results, n);
                    DisplayResult(aggregate);
                }
                else
                {
                    Console.WriteLine("Single thread ...");
                    Console.WriteLine("--------------");
                    var result = RunSimulation(n);
                    DisplayResult(result);
                }
            }

            var time = DateTime.Now - start;
            Console.WriteLine("------------------------------");
            Console.WriteLine("time: {0} s", time.TotalSeconds);
            Console.ReadKey();
        }

        private CardSimResult AggregateResults(List<CardSimResult> results, int n)
        {
            var aggregate = new CardSimResult(n);

            foreach (CardSimResultType resultType in Enum.GetValues(typeof(CardSimResultType)))
            {
                aggregate.AddResult(resultType, results.Sum(x => x.Totals[resultType]));
            }

            return aggregate;
        }

        private void DisplayResult(CardSimResult result)
        {
            var ev = result.CalcExpectedValues();
            var display = result.DisplayExpectedValues();

            Console.Write(display);
        }

        private CardSimResult RunSimulation(int n)
        {
            var deck = new Deck();

            return RunSimulation(n, deck);
        }

        private CardSimResult RunSimulation(int n, int rngSeed)
        {
            var deck = new Deck(rngSeed);

            return RunSimulation(n, deck);
        }

        private CardSimResult RunSimulation(int n, Deck deck)
        {
            var result = new CardSimResult(n);
            int sum2Total = 0, sum3Total = 0, sum4Total = 0;
            int min2 = 0, min3 = 0, min4 = 0, max = 0, sumMax2 = 0, sumMax3 = 0;
            int min2Total = 0, min3Total = 0, min4Total = 0;
            int max1Total = 0, sumMax2Total = 0, sumMax3Total = 0;
            Card c1, c2, c3, c4;

            for (int i = 0; i < n; i++)
            {
                var hand = deck.Deal(4);

                c1 = hand[0];
                c2 = hand[1];
                c3 = hand[2];
                c4 = hand[3];

                sum2Total += (c1.Value + c2.Value);
                sum3Total += (c1.Value + c2.Value + c3.Value);
                sum4Total += (c1.Value + c2.Value + c3.Value + c4.Value);

                min2 = Min(new List<int> { c1.Value, c2.Value });
                min3 = Min(new List<int> { c1.Value, c2.Value, c3.Value });
                min4 = Min(new List<int> { c1.Value, c2.Value, c3.Value, c4.Value });

                min2Total += min2;
                min3Total += min3;
                min4Total += min4;

                max = (c1.Value + c2.Value - min2);
                sumMax2 = (c1.Value + c2.Value + c3.Value - min3);
                sumMax3 = (c1.Value + c2.Value + c3.Value + c4.Value - min4);

                max1Total += max;
                sumMax2Total += sumMax2;
                sumMax3Total += sumMax3;

                deck.Shuffle();
            }

            result.AddResult(CardSimResultType.Max1, max1Total);
            result.AddResult(CardSimResultType.Min2, min2Total);
            result.AddResult(CardSimResultType.Min3, min3Total);
            result.AddResult(CardSimResultType.Min4, min4Total);
            result.AddResult(CardSimResultType.Sum2, sum2Total);
            result.AddResult(CardSimResultType.Sum3, sum3Total);
            result.AddResult(CardSimResultType.Sum4, sum4Total);
            result.AddResult(CardSimResultType.SumMax2, sumMax2Total);
            result.AddResult(CardSimResultType.SumMax3, sumMax3Total);

            int sum3WithReturn = 0, sum3WithReturnTotal = 0;

            deck.Shuffle();

            for (int i = 0; i < n; i++)
            {
                var hand = deck.Deal(3);
                hand.Sort(CompareCards);

                c1 = hand[0];
                c2 = hand[1];
                c3 = hand[2];

                deck.ReturnCard(c1);
                hand.RemoveAt(0);

                c1 = deck.DealCard();

                sum3WithReturn = c1.Value + c2.Value + c3.Value;
                sum3WithReturnTotal += sum3WithReturn;

                deck.Shuffle();
            }

            result.AddResult(CardSimResultType.SumWithReturn, sum3WithReturnTotal);

            return result;
        }

        private int Max(IEnumerable<int> values)
        {
            return values.Max();
        }

        private int Min(IEnumerable<int> values)
        {
            return values.Min();
        }

        private int CompareCards(Card a, Card b)
        {
            int comparison = 0;

            comparison = a.Value - b.Value;

            return comparison;
        }

        private int Sum(IEnumerable<int> values)
        {
            return values.Sum();
        }
    }

    public class CardSimResult
    {
        private int _n = 1;
        private Dictionary<CardSimResultType, int> _totals = new Dictionary<CardSimResultType, int>();
        private Dictionary<CardSimResultType, double> _expectedValues = new Dictionary<CardSimResultType, double>();
        private bool _expectedValuesCalculated = false;

        public CardSimResult(int n)
        {
            _n = n;
        }

        public int N
        {
            get { return _n; }
        }

        public Dictionary<CardSimResultType, int> Totals
        {
            get { return _totals; }
        }

        public void AddResult(CardSimResultType type, int value)
        {
            if (!_totals.ContainsKey(type))
                _totals.Add(type, value);
        }

        public Dictionary<CardSimResultType, double> CalcExpectedValues()
        {
            if (!_expectedValuesCalculated)
            {
                _expectedValues.Clear();

                foreach (var entry in _totals)
                {
                    _expectedValues[entry.Key] = (double)entry.Value / _n;
                }
                _expectedValuesCalculated = true;
            }

            return _expectedValues;
        }

        public string DisplayExpectedValues()
        {
            var sb = new StringBuilder();

            if (_expectedValuesCalculated)
            {
                sb.AppendLine(string.Format("EV[sum2] = {0}", _expectedValues[CardSimResultType.Sum2]));
                sb.AppendLine(string.Format("EV[sum3] = {0}", _expectedValues[CardSimResultType.Sum3]));
                sb.AppendLine(string.Format("EV[sum4] = {0}", _expectedValues[CardSimResultType.Sum4]));
                sb.AppendLine("--------------");
                sb.AppendLine(string.Format("EV[min2] = {0}", _expectedValues[CardSimResultType.Min2]));
                sb.AppendLine(string.Format("EV[min3] = {0}", _expectedValues[CardSimResultType.Min3]));
                sb.AppendLine(string.Format("EV[min4] = {0}", _expectedValues[CardSimResultType.Min4]));
                sb.AppendLine("--------------");
                sb.AppendLine(string.Format("EV[max] = {0}", _expectedValues[CardSimResultType.Max1]));
                sb.AppendLine(string.Format("EV[sumMax2] = {0}", _expectedValues[CardSimResultType.SumMax2]));
                sb.AppendLine(string.Format("EV[sumMax3] = {0}", _expectedValues[CardSimResultType.SumMax3]));
                sb.AppendLine("--------------");
                sb.AppendLine(string.Format("EV[sum3WithReturn] = {0}", _expectedValues[CardSimResultType.SumWithReturn]));
            }
            else
            {
                sb.AppendLine("Should call CalcExpectedValues first.");
            }

            return sb.ToString();
        }
    }

}
