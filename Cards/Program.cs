﻿using System;

namespace Cards
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter number of simulations to be run (integer):");
            var input = Console.ReadLine();
            int n;
            var cs = new CardsSim();

            if (!string.IsNullOrEmpty(input) && int.TryParse(input, out n))
            {
                Console.WriteLine("Single or multi thread (0=single, 1=multi):");
                var type = Console.ReadLine();
                CardSimType simType;

                if (Enum.TryParse(type, out simType))
                    cs.Run(n, simType);

                else
                    cs.Run(n);
            }
            else
            {
                cs.Run();
            }
        }
    }
}
